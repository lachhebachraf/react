import { useEffect, useState } from "react"
import Increment from "./Increment"
import Title from "./Title"

function App() {
  // redux
  let [number, setNumber] = useState(0)

  function increment() {
    setNumber(old => old + 1)
  }

  return (
    <>
      <Title number={number} />
      <Increment increment={increment} />
    </>
  )
  
}

export default App
