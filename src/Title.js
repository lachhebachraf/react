import { useEffect, useState } from "react"

function Title({number}) {

    let [htmlColor, setHtmlColor] = useState("#000000")
    let dynamic = "hamada"

    useEffect(() => {
        if (number >= 5) {
            setHtmlColor("#EF2323")
        }
    }, [number])

    return (
        <>
            <h1 style={{ color: htmlColor }}>number: {number}</h1>
        </>
    )
}

export default Title
